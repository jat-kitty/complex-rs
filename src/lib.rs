use std::ops::{Mul, Div, Add, Sub};


#[derive(Debug, PartialEq)]
pub struct Complex {
	pub real: f64, 
	pub imaginary: f64,
}

impl Complex {
	pub fn new(r: f64, i: f64) -> Complex {
		Complex {
			real: r,
			imaginary: i,
		}
	}

	pub fn sqrt(&self) -> Complex {
		let (a, b) = (self.real, self.imaginary);

		let re = (a + (a.powi(2) + b.powi(2)).sqrt()).sqrt();
		let im = b.signum() * (0_f64 - a + (a.powi(2) + b.powi(2)) / 2_f64).sqrt();

		Complex::new(re, im)
	}
}


impl Add for Complex {
	type Output = Self;

	fn add(self, rhs: Self) -> Self {
		Complex::new(self.real + rhs.real, self.imaginary + rhs.imaginary)
	}
}

impl Sub for Complex {
	type Output = Self; 
	
	fn sub(self, rhs: Self) -> Self {
		Complex::new(self.real - rhs.real, self.imaginary - rhs.imaginary)
	}
}

impl Mul for Complex {
	type Output = Self;

	fn mul(self, rhs: Self) -> Self {
		// (a + bi) * (x + yi) = (ax - by) + (bx + ay)i
		let (a, b) = (self.real, self.imaginary);
		let (x, y) = (rhs.real, rhs.imaginary);
		let re = a * x - b * y;
		let im = b * x + a * y;
		
		Complex::new(re, im)
	}
}

impl Div for Complex {
	type Output = Self;

	fn div(self, rhs: Self) -> Self {
		// (a + bi) / (x + yi) = ((ax + by) / (x^2 + y^2)) + ((bx + ay) / (x^2 + y^2))i
		let (a, b) = (self.real, self.imaginary);
		let (x, y) = (rhs.real, rhs.imaginary);

		let re = (a * x + b * y) / (x.powi(2) + y.powi(2));
		let im = (b * x + a * y) / (x.powi(2) + y.powi(2));
		
		Complex::new(re, im)
	}
}
